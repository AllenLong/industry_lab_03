package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */private String UpperBound;
       private String LowerBound;


    private void start() {

        System.out.println("Provide an upperbound: ");
        UpperBound= Keyboard.readInput();
        System.out.println("Provide an lowerbound: ");
        LowerBound= Keyboard.readInput();

        int UpBound = Integer.parseInt(UpperBound);
        int LowBound = Integer.parseInt(LowerBound);
             System.out.println("The UpperBound is "+ UpBound + " and the LowBound is "+ LowBound);

        int num1 = getRandomNum(LowBound,UpBound);
        int num2 = getRandomNum(LowBound,UpBound);
        int num3 = getRandomNum(LowBound,UpBound);
        System.out.println("The numbers are "+ num1 +"," + num2 + "," +num3);
        int smallNum1 = Math.min(num1,num2);
        int smallest = Math.min(smallNum1,num3);
        System.out.println("The smallest number is " + smallest);
    }

    public int getRandomNum(int LowBound, int UpBound) {
        return (int) Math.round(LowBound + (Math.random() * (UpBound - LowBound)));
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
